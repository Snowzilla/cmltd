<create-user>
	<div class="create-user">

        <!--Creating the text input section-->
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">First Name</label>
                	<div class="col-md-9">
                		<input id="firstName" type="text" class="form-control" placeholder="e.g. Mario">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Last Name</label>
                    <div class="col-md-9">
                        <input id="lastName" type="text" class="form-control" placeholder="e.g. Maxwell">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Year entry into the program</label>
                    <div class="col-md-9">
                        <input id="dob" type="date" class="form-control" placeholder="e.g. 2014">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Program degree level</label>
                    <div class="col-md-9">
                        <input id="school" type="text" class="form-control" placeholder="e.g. MA, EdM, PhD, EdD, Certificate: Hugo Newman">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">TC email</label>
                    <div class="col-md-9">
                        <input id="email" type="email" class="form-control" placeholder="mario@tc.columbia.edu">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Username</label>
                    <div class="col-md-9">
                        <input id="createUsername" type="text" class="form-control" placeholder="input username">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Password</label>
                    <div class="col-md-9">
                        <input id="createPassword" type="password" class="form-control" placeholder="input password">
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Enter Password Again</label>
                    <div class="col-md-9">
                        <input id="createPassword" type="password" class="form-control" placeholder="input password">
                    </div>
            </div>         
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Choose your Target Career Track</label>
                    <div class="col-md-9">
                        <select id="careerTrack" class="form-control">
                            <option value="None">None</option>
                            <option value="Instructional Designer">Instructional Designer</option>
                            <option value="Ph.D/Researcher">Ph.D/Researcher</option>
                            <option value="Technology Specialist">Technology Specialist</option>
                            <option value="Game Designer">Game Designer</option>
                            <option value="Web Developer">Web Developer</option>
                            <option value="Entrepreneur">Entrepreneur</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
            </div>
             <div class="form-group">
                <div class="col-md-offset-6">
                    <button id="saveUser" class="btn btn-success" type="button" onclick={ saveUser }>Save</button>
                </div>
            </div>
    	</form>
	</div>

<script>

//Declaring the user class of Parse objects
var User = Parse.Object.extend("User");


//Function to save User
this.saveUser = function(event) {
    var firstName = $('#firstName').val() || undefined;
    var lastName = $('#lastName').val() || undefined;
    var dob = $('#dob').val() || undefined;
    var school = $('#school').val() || undefined;
    var email = $('#email').val() || undefined;
    var createUsername = $('#createUsername').val() || undefined;
    var createPassword = $('#createPassword').val() || undefined;
    var careerTrack=$('#careerTrack').val();

    var query = new Parse.Query('User');
    if (!/@tc\.columbia\.edu$/.test(email)){
        alert('Must be a tc.columbia.edu email');
        return;
    }
    query.equalTo("username", createUsername);
    query.find().then(function(results) {
        if (createPassword) {
            var user = new User({
                firstName: firstName,
                lastName: lastName,
                dob: dob,
                school: school,
                email: email,
                username: createUsername,
                password: createPassword,
                careerTrack: careerTrack
            });

            user.save().then(function(user) {
                alert("New object created with objectID: " + user.id);
                 Parse.User.logIn(createUsername, createPassword, {
                 success: function() {
                 location.href="index.html";

                 }})
            }, function(error) {
                alert("Failed to create new object: " + error.message);
            });

        } else {
            alert("You must enter a password.");
        }
    });
}

//Form Reset function
function resetForm() {
    $('input[type="text"]').val('');
    $('input[type="password"]').val('');
    $('input[type="date"]').val('');
    $('input[type="email"]').val('');
}
</script>


<style type="text/css">
	.create-user{
        color:white;
    }
</style>

</create-user>
