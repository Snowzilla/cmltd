<headbar>
<!-- exists in every page for navigation -->
    <div class="navcontainer">
        <nav class="navbar navbar-inverse">
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="./index.html">Home</a></li>
                    <li><a href="./about.html">About Us</a></li>
                    <li><a href="./improve.html">Help us improve</a></li>
                    <li><a href="./course_review.html">Course Catalog</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Career Track<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="track_review.html?track=designer">Instructional Designer</a></li>
                            <li><a href="track_review.html?track=researcher">Ph.D/Researcher</a></li>
                            <li><a href="track_review.html?track=tech-specialist">School Tech Specialist</a></li>
                            <li><a href="track_review.html?track=gameDesigner">Game Designer</a></li>
                            <li><a href="track_review.html?track=entrepreneur">Entrepreneur</a></li>
                            <li><a href="track_review.html?track=webDev">Web Developer</a></li>

                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right" id="beforeLoginSection">
                    <li><a href="./create-user.html">New? Join us!</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Login <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <div class="col-md-6 login">
                                <div class="form-horizontal">
                                     <div class="form-group">
                                        <label class="control-label" for="playerName">Username</label>
                                         <input id="loginUsername" type="text" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="playerName">Password</label>
                                        <input id="loginPassword" type="password" class="form-control" placeholder="">
                                    </div>
                                    <div>
                                        <button id="loginButton" class="btn btn-primary btn-block" type="button" onclick={loginUser}>Login</button>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right" id="AfterLoggedinSection">
                    <li><a>Welcome! {username}</a></li>
                    <li id="logout"><a href="./user_account.html">Account</a></li>
                    <li id="logout"><a onclick={logoutUser}>Logout</a></li>
                </ul>
            </div>
        </nav>
    </div>

<script>

var that=this;
var currentUser=Parse.User.current();
if(currentUser){
    that.username=currentUser.get("username");
}

// //$(document).ready(function(){
that.on('mount',function() {
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("600");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("600");
            $(this).toggleClass('open');       
        }
    );
    if(that.username){
        $("#beforeLoginSection").css('display','none');           
        $("#AfterLoggedinSection").css('display','inline'); 
    }
    else {
        $("#beforeLoginSection").css('display','inline');          
        $("#AfterLoggedinSection").css('display','none');
    }
    
});

loginUser=function() {
    var username = $('#loginUsername').val() || undefined;
    var password = $('#loginPassword').val() || undefined;

    Parse.User.logIn(username, password, {
        success: function(user) {
            //once they login successfully,put code for what to do next here    
            alert("Yeay, you logged in successfully!");
            that.username=username;
            that.update();
            $("#beforeLoginSection").css('display','none');          
            $("#AfterLoggedinSection").css('display','inline');
        },
        error: function(user, error) {
            // The login failed. Check error to see why.
            alert("Failed to login: " + error.message);
        }
    });
}
logoutUser=function() {
    Parse.User.logOut();
    alert("Now you have successfully logged out!");
    $("#beforeLoginSection").css('display','inline');          
    $("#AfterLoggedinSection").css('display','none');
}

</script>


<style type="text/css">
    .navcontainer{
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 999999;
    }
    .dropdown-menu{
        width:300px;
        padding:10px;
        z-index: 999;
    }
    .mega-dropdown {
      position: static !important;
    }
    .mega-dropdown-menu {
        padding: 20px 0px;
        width: 100%;
        box-shadow: none;
        -webkit-box-shadow: none;
        z-index: 999;
    }
    .mega-dropdown-menu > li > ul {
      padding: 0;
      margin: 0;
    }
    .mega-dropdown-menu > li > ul > li {
      list-style: none;
    }
    .mega-dropdown-menu > li > ul > li > a {
      display: block;
      color: #222;
      padding: 3px 5px;
    }
    .mega-dropdown-menu > li ul > li > a:hover,
    .mega-dropdown-menu > li ul > li > a:focus {
      text-decoration: none;
    }
    .mega-dropdown-menu .dropdown-header {
      font-size: 18px;
      color: #ff3546;
      padding: 5px 60px 5px 5px;
      line-height: 30px;
    }
    .form-control{
        width:250px;
    }

    #logout:hover {
        cursor:pointer;
    }
</style>

</headbar>
