<user-info>
	<div class="col-md-4" id="user-info">
        <h4>Alumni name: {username}</h4>
        <h4>Years at TC: {dob}</h4>
            <!-- Make button for linked in, email -->
        <h4>Email: {email}</h4>
        <h4>Courses Reviewed</h4>
        <div class="nodateprompt" show={empty}>
            <p>No data yet.</p>
        </div>
        <ul>
            <li each={courselist}>
                {term}<a onclick={parent.showAReview} style="cursor:pointer;">--{courseid} {coursename}</a>
            </li>
        </ul>
        
    </div>
    <div id="first-prompt" class="col-md-6" show={!course}>
        <h3>Click on a course to see the user's review</h3>
    </div>
    <div id="display-prompt" class="col-md-6" show={error}>
        <h3>Sorry, the user doesn't like to share the review for this course.</h3>
    </div>
    <div id="single-review" class="col-md-6" show={course}>
        <h2 id="courseNum">{course.courseid}</h2>
        <h4 id="courseName">{course.coursename}</h4>
        <h4 id="instructor">Instructor: {course.instructor}</h4>
        <h4 id="courseType">Course Type: {course.coursetype}</h4>
        <p class="question">Hours per week: <font class="answer">{course.hours_per_week}</font></p>
        <p class="question">Any courses you wished you took before this.</p>
        <p class="answer">{course.courses_before_this}</p>
        <p class="question">What future career would this be helpful for?</p>
        <p class="answer">{course.future_career}</p>
        <p class="question">What skills do you need to succeed in this course?</p>
        <p class="answer">{course.skills_needed}</p>
        <p class="question">Describe the style of the instructor.</p>
        <p class="answer">{course.instructor_style}</p>
        <p class="question">Free comments</p>
        <p class="answer">{course.comment}</p>
    </div>

<script>
    var that =this;
	 //get user id
    var text=window.location.search;
    function delineate(str) {
        var theleft = str.indexOf("=") + 1;
        return (str.substring(theleft));
    }
    var user_id=delineate(text);
    //query the user
    var User=Parse.Object.extend("User");
    var userQuery=new Parse.Query(User);
    userQuery.get(user_id).then(function (result) {
        that.user=result;
    	that.username=result.get("username");
    	that.dob=result.get("dob");
    	that.email=result.get("email");
    	that.update();

    }).then(function (result){
    //query courses reviewed by the user
        that.courselist=[];
        var Post=Parse.Object.extend("Post");
        var postQuery=new Parse.Query(Post);
        postQuery.equalTo("author",that.user);
        //sort term
        postQuery.ascending("termCode");
        postQuery.find().then(function (posts){
            if (posts.length>0) {
                for(i=0;i<posts.length;i++){
                    var temp=new Object;;
                    temp.term=posts[i].get("term");
                    temp.courseid=posts[i].get("courseId");
                    temp.coursename=posts[i].get("courseName");
                    temp.star_rating=Number(posts[i].get("star_rating"));
                    temp.TA=posts[i].get("TA");
                    temp.credit=posts[i].get("credit");
                    temp.hours_per_week=posts[i].get("hoursPerWeek");
                    temp.courses_before_this=posts[i].get("coursesBeforeThis");
                    temp.future_career=posts[i].get("futureCareer");
                    temp.skills_needed=posts[i].get("skillsNeeded");
                    temp.instructor_style=posts[i].get("instructorStyle");
                    temp.comment=posts[i].get("comment");
                    temp.author_display=posts[i].get("authorDisplay");
                    temp.date=posts[i].createdAt;
                    that.courselist.push(temp);
                }
                that.update();
            }
            else {
                that.empty=true;
                that.update();
            }
        });
    });
    showAReview(event){
        //get current index of courselist
        var item=event.item;
        var index=this.courselist.indexOf(item);
        console.log(item);
        console.log(index);
        var courseid=that.courselist[index].courseid;
        //mount single review
        var Course=Parse.Object.extend("Course");
        var courseQuery=new Parse.Query(Course);
        courseQuery.equalTo("courseId",courseid);
        courseQuery.first().then(function(course){
            that.courselist[index].coursename=course.get("coursename");
            that.courselist[index].instructor=course.get("instructor");
            that.courselist[index].coursetype=course.get("type");
            
        }).then(function(){
            if(that.courselist[index].author_display !== "anonymous"){
                that.course=that.courselist[index];
                that.error=false;
            }
            else {
                that.course= undefined;
                that.error=true;
            }
            that.update();
        })
    }
    

</script>
<style>
    #user-info {
        margin-left: 20px;
        padding:20px;
        line-height:200%;
    }
    .question {
        font-weight: bold;
        color: blue;
    }
    #single-review{
        padding:20px;
        border-left:solid;
    }
</style>
</user-info>
