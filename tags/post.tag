<post>
	<div id="write-post">
		<form id="review-post">
			<br><br><br>
			<h4>Write your review for this course.</h4><br>
			<font>Overall Rating:</font> <input id="star-rating"><br>
			<fieldset id="questionaire">
				<div id="questions-div">
					<table id="quick-questions">
						<tr>
							<td>1. When did you take this course?</td>
							<td>
								<span>
									Term <select name="term" class="answers">
										<option value="Spring">Spring</option>
										<option value="Fall">Fall</option>
										<option value="Summer">Summer</option>
									</select>
									Year <input class="answers" name="year" type="number" min="2014" max="9999">
								<span>

							</td>
						</tr>
						<tr>
							<td>2. Was there a TA in this course?</td>
							<td>
								<form>
									<input class="answers" type="radio" name="TA" value="Yes"> Yes
									<input class="answers" type="radio" name="TA" value="No"> No
								</form>
							</td>
						</tr>
						<tr>
							<td>3.	How many credits did you register?</td>
							<td>
								<input class="answers" name="credit" type="number" min="0" max="4">
							</td>
						</tr>
						<tr>
							<td>4.	How many hours per week did you put into this course?</td>
							<td>
								<input class="answers" name="hoursPerWeek" type="number" min="0" max="15">
							</td>
						</tr>
						<tr>
							<td>5. Percent of lecture (vs discussion and other in-class activities).</td>
							<td>
								Range from 0 to 100. <input class="answers" name="lecturePercent" type="number" min="0" max="100">%
							</td>
						</tr>
					</table>
					<br>
					<div>
						<p>6. Any courses you wished you took before this one.</p>
						<p>
							<textarea name="coursesBeforeThis" id="coursesBeforeThis" class="answers" placeholder="Please leave the course name/No here if any."></textarea>
						</p>
					</div>
					<div>
						<p>7. What future career would this be helpful for?</p>
						<p>
							<textarea name="futureCareer" id="futureCareer" class="answers" placeholder="eg: game design"></textarea>
						</p>
					</div>
					<div>
						<p>8. What skills do you need to succeed in this course?</p>
						<p>
							<textarea name="skillsNeeded" id="skillsNeeded" class="answers" placeholder="eg: problem solving"></textarea>
						</p>
					</div>
					
					
					<div>
						<p>9. Describe the style of the instructor.</p>
						<p>
							<textarea class="answers" name="instructorStyle" id="instructorStyle" placeholder="eg: passionate, focusing more on theory than practice, etc"></textarea>
						</p>
					</div>
					<div>
						<p>10. Any other comments you'd like to share?</p>
						<p>
							<textarea class="answers" name="comment" id="comment" placeholder="eg, your learning experience of the course, the assignment you did, etc"></textarea>
						</p>
					</div>
				</div>
				
			</fieldset>
		   	<span>
		   		<input id="anonymous-setting" type="checkbox">submit as anonymous. 
		   		<button	id="cancel" onclick={cancelTheReview}>Cancel</button>
		   		<input id="post-submit" type="submit" onclick={postAReview}>
		   		</span>
	   	</form>
	</div>

	<script>
		var that=this;
		this.on('mount', function() {
			$('#star-rating').rating({min:0, max:5, step:0.5, size:'xs'});
		});

        // onsubmit function
		this.postAReview=function() {
		 	event.preventDefault();

		 	var username="";
		 	var text = window.location.search;

			function delineate(str) {
			    var theleft = str.indexOf("=") + 1;
			    return (str.substring(theleft));
			}
			courseId = delineate(text);

            //get overall rating result
			var starrating_result=$('#star-rating').val();
			if (Number(starrating_result) == 0) {
				alert("Please rate the course!");
				return;
			}
			//get the result of questionaire and save it in key_answers as the value of objects
			var key_answers=[{keyword:"term",value:""},{keyword:"TA",value:""},{keyword:"credit",value:""},{keyword:"hoursPerWeek",value:""},{keyword:"lecturePercent",value:""},
			{keyword:"coursesBeforeThis",value:""},{keyword:"futureCareer",value:""},{keyword:"skillsNeeded",value:""},{keyword:"instructorStyle",value:""},{keyword:"comment",value:""}];
			var term=$("[name='term']").val();
			if (term == "") {
				alert("please fill the term before submission!");
				return;
			}
            var termcode="0";
            if (term=="Spring") {
                termcode="1";
            }
            else if(term=="Summer") {
                termcode="2";
            }
            else {
                termcode="3";
            }
			var year=$("[name='year']").val();
			termcode=year.concat(termcode);
			if (year == "") {
				alert("please fill the year before submission!");
				return;
			}
			term=term.concat("-");
			term=term.concat(year);
			var TA=$("[name='TA']").filter(":checked").val();
			if (TA == "") {
				alert("please fill the TA before submission!");
				return;
			}
			var credit=$("[name='credit']").val();
			if (credit == "") {
				alert("please fill the credit before submission!");
				return;
			}
			var hoursPerWeek=$("[name='hoursPerWeek']").val();
			if (hoursPerWeek == "") {
				alert("please fill the hoursPerWeek before submission!");
				return;
			}
			var lecturePercent=$("[name='lecturePercent']").val();
			if (lecturePercent == "") {
				alert("please fill the lecturePercent before submission!");
				return;
			}
			var coursesBeforeThis=$("[name='coursesBeforeThis']").val();
			if (coursesBeforeThis == "") {
				alert("please fill the coursesBeforeThis before submission!");
				return;
			}
			var futureCareer=$("[name='futureCareer']").val();
			if (futureCareer == "") {
				alert("please fill the futureCareer before submission!");
				return;
			}
			var skillsNeeded=$("[name='skillsNeeded']").val();
			if (skillsNeeded == "") {
				alert("please fill the skillsNeeded before submission!");
				return;
			}
			var instructorStyle=$("[name='instructorStyle']").val();
			if (instructorStyle == "") {
				alert("please fill the instructorStyle before submission!");
				return;
			}
			var comment=$("[name='comment']").val();

	    	//save user
	    	var anonymous=document.getElementById("anonymous-setting").checked;
	    	var author=Parse.User.current();
	    	var username=author.get("username");
	    	var author_display=anonymous ? "anonymous":username;

	    	//conect with Parse
			var Post=Parse.Object.extend("Post");
    		var newPost=new Post();
    		newPost.save({
    			starRating: Number(starrating_result),
    			term:term,
    			TA:TA,
    			credit:credit,
    			hoursPerWeek:hoursPerWeek,
    			lecturePercent:lecturePercent,
    			coursesBeforeThis:coursesBeforeThis,
    			futureCareer:futureCareer,
    			skillsNeeded:skillsNeeded,
    			instructorStyle:instructorStyle,
    			comment:comment,
    			author:author,
    			authorDisplay:author_display,
    			courseId: courseId, 
    			termCode:termcode
    		},{
    			success:{},
    			error:function(error){
    				alert('error:'+error.message+error.code);
    				return;
    			}
    		}).then(function () {
    			var  Post=Parse.Object.extend("Post");
    			var postQuery=new Parse.Query(Post);
    			postQuery.equalTo("courseId",courseId);
    			postQuery.find().then(function (results){
    				var post_length=results.length;
    				averageRating=0;
    				for (i=0;i<post_length;i++) {
    		    		averageRating+=results[i].get('starRating');
    				}
    				averageRating=averageRating/post_length;
    				var Course=Parse.Object.extend("Course");
	    			var queryCourse=new Parse.Query(Course);
	    			queryCourse.equalTo("courseId",courseId);
	    			queryCourse.first().then(function(thisCourse) {
	    				thisCourse.set('averageRating',averageRating);
	    				thisCourse.save(null,{
	    					success: function(){
	    						alert("Your review has been successfully submitted!"); 		
								that.unmount();
	    					}, error:function(error){
	    						alert('error:'+error.message+error.code);
	    						return;
	    					}
	    				});

	    			});
    			});
    			

    		});
	    }
//NEED DEVELOPMENT!!
	    // functions to reset forms
		this.cancelTheReview= function() {
			//$('.answers').attr('checked',false);
			//$('.answers').val();
			$('#write-post').hide();
		}

	</script>

	<style scoped>
		:scope {
			display: block;
			background-color: white;
		};

		#write-post {
			display: block;
			margin: 10px;
			border:solid;
			padding:10px;
		};
		#post-form {
			margin-left: 10px;
			border: 1px solid green;
			padding: 10px;
		}
		.post {
			border: 1px solid blue;
			padding: 10px;
			margin: 15px 0;
		};
		#list-posts{
			border: 1px solid green;
			padding: 10px;
			margin: 15px 0;

		}
		textarea{
			height:30px;
			width:400px;
		}
		#a-9 {
			height:80px;
			width:400px;
		}
		#post-title{


		}
		#post-content{
			height:20px;
			width:500px;

		}
		#anonymous-setting{
			margin-right: 10px;
		}
		#post-submit{
			margin-left: 10px;
		}
		#instructorStyle{
			height:100px;
			width:550px;
		}
		#comment{
			height:150px;
			width:550px;
		}
		#coursesBeforeThis{
			height:50px;
			width:550px;
		}
		#skillsNeeded{
			height:50px;
			width:550px;
		}
		#futureCareer{
			height:50px;
			width:550px;
		}
		td{
			height:30px;
		}
	</style>
</post>