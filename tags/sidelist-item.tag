<sidelist-item>
<!-- child of <side-list>, display content in each course number -->
	<a href={ref} class="title-2" onclick={toggleList}><font color="white"> {coursenumber} {coursename} </font></a>
	<!--add stars after each course. change -1X to change size and change color in <style>-->
	<span class="star">
	    <i class="fa fa-1x { fa-star-o:(preview<0.5), fa-star-half-o:(preview>=0.5 && preview<1), fa-star:(preview>=1)}"></i>
	    <i class="fa fa-1x { fa-star-o:(preview < 1.5), fa-star-half-o:(preview>=1.5 && preview<2), fa-star:(preview>= 2) }"></i>
	    <i class="fa fa-1x { fa-star-o:(preview < 2.5), fa-star-half-o:(preview>=2.5 && preview<3), fa-star:(preview >= 3) }"></i>
	    <i class="fa fa-1x { fa-star-o:(preview < 3.5), fa-star-half-o:(preview>=3.5 && preview<4), fa-star:(preview >= 4) }"></i>
	    <i class="fa fa-1x { fa-star-o:(preview < 4.5), fa-star-half-o:(preview>=4.5 && preview<5), fa-star:(preview >= 5) }"></i>
  	</span>
  	<!-- If there are more than one course under this course number, show the sublist of the course number-->
	<ul class="sublist">
		<li each={sublist}><a href={ref}><font color="white">{section} {instructor} {coursetype}</font></a>
		<span class="star">
		    <i class="fa fa-1x { fa-star-o:(preview<0.5), fa-star-half-o:(preview>=0.5 && preview<1), fa-star:(preview>=1)}"></i>
		    <i class="fa fa-1x { fa-star-o:(preview < 1.5), fa-star-half-o:(preview>=1.5 && preview<2), fa-star:(preview>= 2) }"></i>
		    <i class="fa fa-1x { fa-star-o:(preview < 2.5), fa-star-half-o:(preview>=2.5 && preview<3), fa-star:(preview >= 3) }"></i>
		    <i class="fa fa-1x { fa-star-o:(preview < 3.5), fa-star-half-o:(preview>=3.5 && preview<4), fa-star:(preview >= 4) }"></i>
		    <i class="fa fa-1x { fa-star-o:(preview < 4.5), fa-star-half-o:(preview>=4.5 && preview<5), fa-star:(preview >= 5) }"></i>
  		</span> 
  		</li>
	</ul>

	
<script type="text/javascript">
if(this.line.length){

	if (this.line.length==1) {
		this.coursename=this.line[0].get("courseName");
		this.coursenumber=this.line[0].get("courseNo");
		this.instructor=this.line[0].get("instructor");
		this.starrating=this.line[0].get("averageRating");
		this.preview=this.starrating;
		this.courseid=this.line[0].get("courseId");
		this.id=this.line[0].id;
		var ref="course_review.html?courseId=";
		this.ref=ref.concat(this.courseid);

	}
	else{
		this.coursename=this.line[0].get("courseName");
		this.coursenumber=this.line[0].get("courseNo");
		this.sublist=[];
		for (i=0;i<this.line.length;i++){
			this.sublist[i]=new Object;
			this.sublist[i].section=this.line[i].get("courseSection");
			this.sublist[i].instructor=this.line[i].get("instructor");
			this.sublist[i].coursetype=this.line[i].get("type");
			this.sublist[i].starrating=this.line[i].get("averageRating");
			this.sublist[i].preview=this.sublist[i].starrating;
			this.sublist[i].courseid=this.line[i].get("courseId");
			this.sublist[i].id=this.line[i].id;
			var ref="course_review.html?courseId=";
			this.sublist[i].ref=ref.concat(this.sublist[i].courseid);
		}
	}
}
var that=this;
// toggle the sublist
this.toggleList=function(e) {
	if (that.ref){
		location.href=that.ref;
	}
	else{
		$(e.currentTarget).next().next('ul').slideToggle(200);
		console.log(e.currentTarget);
	}	
}
this.on("mount",function(){
	$(".sublist").hide();

});


</script>
<style>
	.star {
		color:yellow;
		font-size:1em;
	}
</style>

</sidelist-item>