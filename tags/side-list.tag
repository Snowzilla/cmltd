<side-list>
<!-- Part of course_review.html -->
    <div class="container">
        <div id="courselist" class="col-md-6">
        	<div>
        		<span>
        			<!-- filter for course catalog -->
        			<font><strong id="filterWord">Filter</strong></font>  <select id="filter">
        				<option value="None">None</option>
        				<option value="Required">Required</option>
        				<option value="Elective">Elective</option>
        				<option value="Breadth">Breadth</option>
        			</select>
        			<button onclick={filterTheList}>Go</button>
                    <!-- add a course -->
                    <add-a-course></add-a-course>
        	</div>
        	<!-- show the course list -->
            <div each={list, index in lists} id="{list_id[index]}" show="{list.exist}">
                <a href="#" class="title-1" onclick={toggleList}><h4>{keywords[index]}</h4></a>
                <ul>
                    <li riot-tag="sidelist-item" each={line in list}></li>
                </ul>
            </div>

        </div>
    </div>

<script>

    this.keywords=["Required","Cognitive and Learning Sciences Issues and Technology",
	    "Social Issues and Technology","Cultural Issues and Technology","Educational Practice and Design",
	    "Elective","Breadth"];
	this.list_id=["required-list","cognitive-list","social-list","cultural-list","design-list","elective-list","breadth-list"];
	var self=this;
    init();
    function init(){
    	var Course=Parse.Object.extend("Course");
	    var courseQuery=new Parse.Query(Course);
	    var filter=$("#filter").val();
	    self.filtered=false;
	    if (filter !== undefined && filter !=="None") {
	    	courseQuery.equalTo("keywords",filter);
	    	self.filtered=true;

	    }
	    courseQuery.ascending("courseNo");
	    
	    var lists=[];
	    courseQuery.find().then(function (results){
	    	if (self.filtered) {
	    		self.keywords=[filter];
	    	}
	    	else {
	    		self.keywords=["Required","Cognitive and Learning Sciences Issues and Technology","Social Issues and Technology","Cultural Issues and Technology","Educational Practice and Design","Elective","Breadth"];
	    	}
	    	for (l=0;l<self.keywords.length;l++) {
		        lists[l]=[];
		        for(i=0;i<results.length;i++) {
		            var current_course=undefined;
		            var keywords_each=results[i].get("keywords");
		            var list_temp=[];
		            var flag=false;
		            for (j=0;j<keywords_each.length;j++) {
		                if (keywords_each[j]==self.keywords[l]) {
		                    list_temp.push(results[i]);
		                    flag=true; // the course belongs to current list 
		                    current_course=results[i].get("courseNo");
		                    break;                   
		                }
		            }
		            if(flag==true){ 
		                //then find other courses with same course number
		                for(j=i+1;j<results.length;j++) {
		                    if (results[j].get("courseNo") == current_course){
		                        list_temp.push(results[j]);
		                    }
		                    else {
		                        break;
		                    }
		                }
		                i=j-1;
		            }
		            if(list_temp.length>0){
		                lists[l].push(list_temp);
		            }
		        }        
		    }
		    for (l=0;l<self.keywords.length;l++) {
		        if (lists[l]==[]) {
		            lists.splice(l,1);
		            l=l-1;
		        }
		    }
		    for (l=0;l<self.keywords.length;l++) {
		    	if(lists[l].length==0) {
		    		lists[l].exist=false;
		    	}
		    	else {
		    		lists[l].exist=true;
		    	}
		    }
		   self.lists=lists;
		   self.update();
	    });
    }
    this.filterTheList=function(){
    	init();
    }
    this.toggleList=function(e) {
		$(e.currentTarget).next('ul').slideToggle(200);
		console.log(e.currentTarget);	
	}
       
</script>

<style >

 #courselist {
 	background-color: #337AB7;
 	/*background-color:#0099cc;*/
 	padding-left: 0px;
 	padding-top: 15px;
 }
 h3 {
 	color:#ffffff;
 }
 select{
 	width:150px;
 }

#filterWord{
	margin-left: 30px;
	color: #ACAAAB;
}
    
</style>
</side-list>