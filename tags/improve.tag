<improve>
	<div>
		<h3>Leave us a message. Any feedback helps!</h3>
        <form class="form-horizontal">
           	<div class="form-group">
               	<label class="col-md-3 control-label">Name</label>
               	<div class="col-md-9">
               		<input id="name" type="text" class="form-control" placeholder="e.g. Mario">
               	</div>
           	</div>
           	
           	<div class="form-group">
               	<label class="col-md-3 control-label">Email address</label>
                   <div class="col-md-9">
                       <input id="email" type="email" class="form-control" placeholder="mario@tc.columbia.edu">
                   </div>
           	</div>
           	<!-- <div class="form-group">
               	<label class="col-md-3 control-label">Comment</label>
               	<div class="col-md-9">
               		<input type="text" class="form-control" placeholder="Say hello!">
               	</div>
           	</div> -->
           	<div class="form-group">
           		<label class="col-md-3 control-label">Comment</label>
           		<textarea id="commentBox" name="comment" form="usrform" placeholder="Enter comment here..."></textarea>
           	</div>
          
            	<div class="form-group">
               	<div class="col-md-offset-6">
                   	<button id="saveComment" class="btn btn-success" type="button" onclick={saveComment }>Save</button>
               	</div>
           	</div>
    	</form>
	</div>

<script type="text/javascript">
	//Declaring the user class of Parse objects
var Comment = Parse.Object.extend("Comment");


//Function to save comment
this.saveComment = function(event){
	var name = $('#name').val() || undefined;
    var email = $('#email').val() || undefined;
    var commentBox = $('#commentBox').val() || undefined;

    var comment = new Comment({
    	name: name,
    	email: email,
    	comment: commentBox
    });
    
    comment.save().then(function(event){
            alert("Thanks for leaving us feedback!");
            location.href="index.html";
        }, function(error) {
            alert("Failed to create comment. Error: " + error.message);
        });
};

</script>

<style type="text/css">
	textarea{
		height: 200px;
		width: 400px;
		max-height: 200px;
		max-width: 400px;
		border-radius: 5px;
		margin-left: 15px;
	}
</style>
</improve>