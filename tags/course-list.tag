<course-list>
<!-- Show courses in track_review.html -->
    <div class="container">
        <h3>Top 10 relevant courses</h3>
        <div class="nodateprompt" show={empty}>
            <h4>No data yet.</h4>
        </div>   
        <ul>
            <li each={course in courselist}>
                <a href={course.ref}>{course.coursenumber} {course.coursename} {course.instructor}</a>
                <!-- show stars rating results -->
                <span class="star">
                    <i class="fa fa-1x { fa-star-o:(course.preview<0.5), fa-star-half-o:(course.preview>=0.5 && course.preview<1), fa-star:(course.preview>=1)}"></i>
                    <i class="fa fa-1x { fa-star-o:(course.preview < 1.5), fa-star-half-o:(course.preview>=1.5 && course.preview<2), fa-star:(course.preview>= 2) }"></i>
                    <i class="fa fa-1x { fa-star-o:(course.preview < 2.5), fa-star-half-o:(course.preview>=2.5 && course.preview<3), fa-star:(course.preview >= 3) }"></i>
                    <i class="fa fa-1x { fa-star-o:(course.preview < 3.5), fa-star-half-o:(course.preview>=3.5 && course.preview<4), fa-star:(course.preview >= 4) }"></i>
                    <i class="fa fa-1x { fa-star-o:(course.preview < 4.5), fa-star-half-o:(course.preview>=4.5 && course.preview<5), fa-star:(course.preview >= 5) }"></i>
                </span>
            </li>
        </ul>
    </div>

<script>
    //the courses needed to show 
    this.courses=this.opts.courses;
    this.courselist=[];
    if (this.courses.length==0) {
        this.empty=true;
    }   
    for(i=0;i<this.courses.length;i++){
        var temp=new Object;
        temp.coursename=this.courses[i].get("courseName");
        temp.coursenumber=this.courses[i].get("courseNo");
        temp.instructor=this.courses[i].get("instructor");
        temp.starrating=this.courses[i].get("averageRating");
        temp.preview= temp.starrating;
        temp.courseid=this.courses[i].get("courseId");
        temp.id=this.courses[i].id;
        var ref="course_review.html?courseId=";
        temp.ref=ref.concat(temp.courseid);
        this.courselist.push(temp);
    }
    console.log(this.courselist);
    console.log(this.courses);
    
    
    
    
</script>

<style>
    
</style>

</course-list>