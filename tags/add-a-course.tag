<add-a-course>
<!-- Child of <side-list>, functioning for users to recommend a new course into course list -->
    <!-- Click the button to trigger the function -->
    <button type="button" class="btn btn-default" id="recButton" data-toggle="modal" onclick={showModal}>Recommend a course</button>
    <!-- Modal to show the section for adding a course -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Recommend a Course</h4>
                </div>
            <div class="modal-body">
                <form id="basic-info">
                    <div class="form-group">
                        <label for="coursenumber">Course Number</label>
                        <input type="text" class="form-control" id="coursenumber" placeholder="eg:MSTU4000">
                    </div>
                    <div class="form-group">
                        <label for="coursename">Course Name</label>
                        <input type="text" class="form-control" id="coursename" placeholder="Course Name">
                    </div>
                    <div class="form-group">
                        <label for="instructor">Instructor</label>
                        <input type="text" class="form-control" id="instructor" placeholder="Instructor Name">
                    </div>
                    <div class="form-group">
                        <label for="type">Course Type</label>
                        <select class="form-control" id="type">
                            <option value="face to face">face to face</option>
                            <option value="online">online</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="class">Course Class</label>
                        <select id="class" class="form-control">
                            <option value="Elective">Elective</option>
                            <option value="Breadth">Breadth</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="careertrack">Career Track</label>
                        <select id="careertrack" class="form-control">
                            <option value="None" selected={track[0]}>None</option>
                            <option value="Instructional Designer">Instructional Designer</option>
                            <option value="Ph.D/Researcher">Ph.D/Researcher</option>
                            <option value="Technology Specialist">Technology Specialist</option>
                            <option value="Game Designer">Game Designer</option>
                            <option value="Web Developer">Web Developer</option>
                            <option value="Entrepreneur">Entrepreneur</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick={close}>Close</button>
                <button type="button" class="btn btn-primary" onclick={submit}>Submit</button>
            </div>
        </div>
    </div>
    <script>
        var self=this;
        self.track_short=self.opts.track;
        self.showModal=function() {
            //check if the user has logged in.
            var checkLogin=Parse.User.current();
            if (checkLogin) {
                $("#myModal").modal();
            }
            else {
                alert("Please login first.");
                return;
            }
        };
        //Close function: clear the form and hide it.
        self.close=function(){
            resetform();
            $("#myModal").modal('toggle');
        }
        function resetform() {
            $('#coursename').val('');
            $('#instructor').val('');
            $('#coursenumber').val('');
            $('#type').val('');
        }
        self.submit=function() {
            // Check if the form has been filled
            var coursename=$('#coursename').val();
            if (coursename=="") {
                alert("Please fill the course name!");
                return;
            }
            var instructor=$('#instructor').val();
            if (instructor=="") {
                alert("Please fill the instructor!");
                return;
            }
            var coursenumber=$('#coursenumber').val();
            if (coursenumber=="") {
                alert("Please fill the course number!");
                return;
            }
            var coursetype=$('#type').val();
            if (coursetype=="") {
                alert("Please fill the course type!");
                return;
            }
            var Course=Parse.Object.extend("Course");
            var courseQuery=new Parse.Query(Course);
            var courseclass=[];
            courseclass[0]=$('#class').val();
            if (courseclass[0]=="") {
                alert("Please fill the course class!");
                return;
            }
            var careertrack=[];
            careertrack[0]=$('#careertrack').val();
            if (careertrack[0]=="") {
                alert("Please fill the career track!");
                return;
            }
            courseQuery.equalTo("courseNo",coursenumber);
            courseQuery.equalTo("instructor",instructor);
            courseQuery.equalTo("type",coursetype);
            courseQuery.find().then(function(results){
                if (results.length>0){
                //The course has already existed.
                    alert("The course already exists!");
                    return;
                }
                else{
                //If the course doesn't exist, save the course to Parse.
                    var course=new Course();
                    course.set("courseName",coursename);
                    course.set("courseNo",coursenumber);
                    course.set("instructor",instructor);
                    course.set("type",coursetype);
                    course.set("keywords",courseclass);
                    course.set("tracks",careertrack);
                    //assign courseSection for the course
                    var newQuery=new Parse.Query(Course);
                    newQuery.equalTo("courseNo",coursenumber
                        );
                    newQuery.find().then(function(results){
                        var section=results.length+1;
                        var coursesection="00";
                        coursesection=coursesection.concat(section);
                        course.set("courseSection",coursesection);
                        var courseid=coursenumber;
                        courseid=courseid.concat(".");
                        courseid=courseid.concat(coursesection);
                        course.set("courseId",courseid);
                        course.save(null,{
                            success: function(){
                                alert("Your recommendation has been submitted! Hit OK and refresh the page to see the course in the list. Don't forget to review it.");
                            //reset form
                                resetform();
                            //self close
                                $("#myModal").modal('toggle');
                            },
                            error: function(a,b) {
                                alert("saving error!");
                            }
                        });
                    });
                    
                }
            })
        }
    </script>
    <style type="text/css">
        #myModal{
            position:fixed;
            left:400px;
            top:50px;
            height:700px;
            width:700px;
        }

        #recButton{
            margin-left: 50px;
        }
    </style>
</add-a-course>






