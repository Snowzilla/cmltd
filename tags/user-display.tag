<user-display>
<!-- Part of track_review.html to show users who are interested in this track as a list. -->
	<div class="container">
		<h3>Users who are interested in this track</h3>
		<div class="nodateprompt" show={empty}>
            <h4>No data yet.</h4>
        </div>
		<ul>
			<li  each={user in userlist}><a href={user.ref}>{user.name}</a></li>
		</ul>
		
	</div>

	
<script type="text/javascript">
	this.users=this.opts.users;
	this.userlist=[];
	if (this.users.length==0) {
        this.empty=true;
    }   
	for(i=0;i<this.users.length;i++){
		var temp=new Object;
		temp.name=this.users[i].get("username");
		temp.id=this.users[i].id;
		var ref="user_info.html?user=";
		temp.ref=ref.concat(temp.id);
		this.userlist.push(temp);
	}

</script>
<style>
	.star {
		color:yellow;
		font-size:1em;

	}
</style>

</user-display>