<user-account>
	<div class="create-user">

        <!--Creating the text input section-->
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">First Name</label>
                	<div class="col-md-9">
                		<span><input id="firstName" class="form-control" value={firstname} disabled={! editmode}>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Last Name</label>
                    <div class="col-md-9"> 
                        <input id="lastName" type="text" class="form-control" value={lastname} disabled={! editmode}>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Year entry into the program</label>
                    <div class="col-md-9">
                        <input id="dob" type="date" class="form-control" value={year} disabled={! editmode}>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Program degree level</label>
                    <div class="col-md-9">
                        <input id="school" type="text" class="form-control" value={degree} disabled={! editmode}>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">TC email</label>
                    <div class="col-md-9">
                        <input id="email" type="email" class="form-control" value={email} disabled={! editmode}>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Username</label>
                    <div class="col-md-9"> 
                        <input id="createUsername" type="text" class="form-control" value={username} disabled={! editmode}>
                    </div>
            </div>
            <!-- <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Password</label>
                     <div class="col-md-9">
                        <input id="createPassword" type="password" class="form-control" placeholder="input password">
                    </div>
            </div> -->
            <!-- <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Enter Password Again</label>
                    <div class="col-md-9">
                        <input id="createPassword" type="password" class="form-control" placeholder="input password">
                    </div>
            </div>          -->
            <div class="form-group">
                <label class="col-md-3 control-label" for="playerName">Your Target Career Track</label>
                    <div class="col-md-9" >
                        <select id="careerTrack" class="form-control" disabled={! editmode}>
                            <option value="None" selected={track[0]}>None</option>
                            <option value="Instructional Designer" selected={track[1]}>Instructional Designer</option>
                            <option value="Ph.D/Researcher" selected={track[2]}>Ph.D/Researcher</option>
                            <option value="Technology Specialist" selected={track[2]}>Technology Specialist</option>
                            <option value="Game Designer" selected={track[3]}>Game Designer</option>
                            <option value="Web Developer" selected={track[4]}>Web Developer</option>
                            <option value="Entrepreneur" selected={track[5]}>Entrepreneur</option>
                            <option value="Other" selected={track[6]}>Other</option>
                        </select>
                    </div>
            </div>
             <div class="form-group">
                <div class="col-md-offset-6">
                    <button id="edit" class="btn btn-success" type="button" onclick={edit}  disabled={editmode}>Edit</button>
                    <button id="saveUser" class="btn btn-success" type="button" onclick={ saveUser }  disabled={! editmode}>Save</button>

                </div>
            </div>
    	</form>
	</div>

<script>

//Declaring the user class of Parse objects
var User = Parse.Object.extend("User");
var user= Parse.User.current();
console.log(user);
if (user) {
    this.id=user.id;
    this.username=user.get("username");
    this.careertrack=user.get("careerTrack");
    this.track=[false,false,false,false,false,false,false];
    var tracks=["None", "Instructional Designer" , "Ph.D/Researcher", "Technology Specialist","Game Designer","Web Developer","Entrepreneur","Other"];
    for (i=0;i<7;i++){
        if(this.careertrack === tracks[i]){
            this.track[i]=true;
            break;
        }
    }
    this.email=user.get("email");
    this.year=user.get("dob");
    this.degree=user.get("school");
    this.firstname=user.get("firstName");
    this.lastname=user.get("lastName");
    this.editmode=false;
    this.update();
}

var that=this;

//Function to save User
this.saveUser = function(event) {
    var firstName = $('#firstName').val() || undefined;
    var lastName = $('#lastName').val() || undefined;
    var dob = $('#dob').val() || undefined;
    var school = $('#school').val() || undefined;
    var email = $('#email').val() || undefined;
    var createUsername = $('#createUsername').val() || undefined;
    var createPassword = $('#createPassword').val() || undefined;
    var careerTrack=$('#careerTrack').val();

    var query = new Parse.Query('User');
    if (!/@tc\.columbia\.edu$/.test(email)){
        alert('Must be a tc.columbia.edu email');
        return;
    }
    query.get(that.id).then(function(user) {
        user.save({
            firstName: firstName,
            lastName: lastName,
            dob: dob,
            school: school,
            email: email,
            username: createUsername,
            careerTrack: careerTrack
        },{
            success: function(user) {
                alert("Successfully updated!");
            },
            error: function(error) {
                alert("Error of updating!");
            }
        });
    });
}
this.edit=function() {
    that.editmode=true;

}

//Form Reset function
function resetForm() {
    $('input[type="text"]').val('');
    $('input[type="password"]').val('');
    $('input[type="date"]').val('');
    $('input[type="email"]').val('');
}
</script>


<style type="text/css">
	.create-user{
        color:white;
    }

</style>

</user-account>
