<coursereview>
<div>
    <div>
        <!-- Basic info of the course -->
        <div class="courseBox">
            <h2 id="courseNum">{course_number}              <button id="post-button" class="btn btn-secondary btn-lg" onclick={postAReview}>Write A Review</button></h2>
            <h4 id="courseName">{course_name}</h4>
            <h4 id="instructor">Instructor: {instructor}</h4>
            <h4 id="courseType">Course Type: {course_type}</h4>
            <h4 id="TA">{TA}</h4>
        </div>
    </div>
    <!-- Show the reviews of the course -->
    <div class="review-display-area" show={! nopost}>
        <!-- Average rating results -->
        <div id="overall-rating-display">
            <p>Average rating from <font>{ reviewer_number }</font> reviewers:
            <span class="star">
                <i class="fa fa-lg { fa-star-o:(rating_result<0.5), fa-star-half-o:(rating_result>=0.5 && rating_result<1), fa-star:(rating_result>=1)}"></i>
                <i class="fa fa-lg { fa-star-o:(rating_result < 1.5), fa-star-half-o:(rating_result>=1.5 && rating_result<2), fa-star:(rating_result>= 2) }"></i>
                <i class="fa fa-lg { fa-star-o:(rating_result < 2.5), fa-star-half-o:(rating_result>=2.5 && rating_result<3), fa-star:(rating_result >= 3) }"></i>
                <i class="fa fa-lg { fa-star-o:(rating_result < 3.5), fa-star-half-o:(rating_result>=3.5 && rating_result<4), fa-star:(rating_result >= 4) }"></i>
                <i class="fa fa-lg { fa-star-o:(rating_result < 4.5), fa-star-half-o:(rating_result>=4.5 && rating_result<5), fa-star:(rating_result >= 5) }"></i>
            </span> {rating_result}</p>
            <!-- Percent of lecture -->
            <p id="percent_discussion">Percent of lecture (vs discussion and other in-class activities):{lecture_percent}%</p>           
        </div>
        <!-- Display individual reviews -->
        <div id="questionaire-display" class="panel-group">
            <div each={posts} class="panel panel-primary">
                <div class="panel-heading">
                    <p>Author: {author_display} ---- {term}  ---- {credit} credits ---- Rating: 
                    <span class="star">
                    <i class="fa fa-lg { fa-star-o:(star_rating<0.5), fa-star-half-o:(star_rating>=0.5 && star_rating<1), fa-star:(star_rating>=1)}"></i>
                    <i class="fa fa-lg { fa-star-o:(star_rating < 1.5), fa-star-half-o:(star_rating>=1.5 && star_rating<2), fa-star:(star_rating>= 2) }"></i>
                    <i class="fa fa-lg { fa-star-o:(star_rating < 2.5), fa-star-half-o:(star_rating>=2.5 && star_rating<3), fa-star:(star_rating >= 3) }"></i>
                    <i class="fa fa-lg { fa-star-o:(star_rating < 3.5), fa-star-half-o:(star_rating>=3.5 && star_rating<4), fa-star:(star_rating >= 4) }"></i>
                    <i class="fa fa-lg { fa-star-o:(star_rating < 4.5), fa-star-half-o:(star_rating>=4.5 && star_rating<5), fa-star:(star_rating >= 5) }"></i>
                </span></p> 
                </div>
                <div class="panel-body">
                    <p class="question">Hours per week: <font class="answer">{hours_per_week}</font></p>
                    <p class="question">Any courses you should take before this.</p>
                    <p class="answer">{courses_before_this}</p>
                    <p class="question">What future career would this be helpful for?</p>
                    <p class="answer">{future_career}</p>
                    <p class="question">What skills do you need to succeed in this course?</p>
                    <p class="answer">{skills_needed}</p>
                    <p class="question">Describe the style of the instructor.</p>
                    <p class="answer">{instructor_style}</p>
                    <p class="question">Free comments</p>
                    <p class="answer">{comment}</p>
                </div>
            </div>
            
        </div>

    </div>

</div>  

<script type="text/javascript">

    // Get id of the course
    var text = window.location.search;

    function delineate(str) {
        var theleft = str.indexOf("=") + 1;
        return (str.substring(theleft));
    }
    var that=this;
    that.nopost=true;
    var courseid=delineate(text);
    if (courseid !=="") {
        var Course = Parse.Object.extend('Course');
        var query = new Parse.Query(Course); 
        query.equalTo("courseId",courseid);
        query.first().then(function(course) {
            // get basic information of the course
            that.course_number=course.get("courseNo");
            that.course_name=course.get("courseName");
            that.instructor=course.get("instructor");
            that.course_type=course.get("type");
            that.course_id=course.get("courseId");
            that.course=course;
            var rating_result=course.get("averageRating");
            if(rating_result) {
                that.rating_result=rating_result.toFixed(1);
            }
            that.update();
        }).then(function(){
            // Get Details of all the posts for the course
            var Post=Parse.Object.extend("Post");
            var postQuery=new Parse.Query(Post);
            postQuery.equalTo("courseId",that.course_id);
            // Sort the posts by time.
            postQuery.descending("createdAt");
            postQuery.find().then(function(posts){
                if (posts.length == 0) {
                    that.nopost=true;
                    that.TA=" ";
                    that.update();
                }
                else {
                //get TA info
                    that.nopost=false;
                    var TA=posts[0].get('TA');
                    if (TA=="Yes"){
                        that.TA="TA: Yes";
                    }
                    else {
                        that.TA="TA: No";
                    }  
                }
                //get rating info
                that.reviewer_number=posts.length;
                //get average percentage of lecture
                var lecture_percent=0;
                var amount=posts.length;
                for (i=0;i<posts.length;i++){
                    var lecture_percent_each=posts[i].get("lecturePercent");
                    if(lecture_percent_each == undefined){
                        amount-=1;
                    }
                    else {
                        lecture_percent+=Number(lecture_percent_each);
                    }          
                }
                if(amount>0){
                    lecture_percent=lecture_percent/amount;
                    that.lecture_percent=lecture_percent.toFixed(0);
                }
                else {
                    lecture_percent="No data yet";
                }
                
                //display each review
                that.posts=[];
                for (i=0;i<posts.length;i++){
                    var temp=new Object;
                    temp.star_rating=Number(posts[i].get("starRating"));
                    temp.term=posts[i].get("term");
                    temp.TA=posts[i].get("TA");
                    temp.credit=posts[i].get("credit");
                    temp.hours_per_week=posts[i].get("hoursPerWeek");
                    temp.courses_before_this=posts[i].get("coursesBeforeThis");
                    temp.future_career=posts[i].get("futureCareer");
                    temp.skills_needed=posts[i].get("skillsNeeded");
                    temp.instructor_style=posts[i].get("instructorStyle");
                    temp.comment=posts[i].get("comment");
                    temp.author_display=posts[i].get("authorDisplay");
                    temp.date=posts[i].createdAt;
                    that.posts.push(temp);
                }
                that.update();
            });
        });
    }
 
    // Post a review
    this.postAReview=function() {
        // Check if the user has logged in
        var checkLogin=Parse.User.current();
        if (checkLogin) {
            //check if the user has made the review
            var Post=Parse.Object.extend("Post");
            var postQuery=new Parse.Query(Post);
            postQuery.equalTo("author",checkLogin);
            postQuery.equalTo("courseId",that.course_id);
            postQuery.first().then(function (result){
                if (result){
                    alert("You have already reviewed this course.");
                }
                else {
                    //Display review section and lead the user to it
                    riot.mount("post");
                    window.location.href="#post-section";
                }
            });         
        }
        else {
            alert("To ensure the authenticity of the review, Please login first.")
        }   
    }

    
   

    
</script>

<style type="text/css">

    h4{
        padding-left: 30px;
    }

    .courseBox{
        background-color: white;
        padding: 20px;
        margin-top: 20px;
    }

    #comments{
        padding-left: 60px;
    }
    .fa-lg {
        color:purple;
    }
    .question {
        color:#0066A4;
        font-weight: bold;
    }
    .answer {
        color:black;
    }
</style>
</coursereview>